# Neues Semester

Zu Beginn einer neuen *[Zeitspanne](stammdaten.md#zeitspanne)* (zB. eines Semesters) müssen einige vorbereitende Datensätze angelegt werden:

1. Falls noch nicht vorhanden, die neue *[Zeitspanne](stammdaten.md#zeitspanne)* anlegen.
2. Die Kursdurchführungen aus der letzten Zeitspanne neu durchführen. Dazu bietet sich die Aktion *[Erneut durchführen](laufende_daten.md#kursdurchfuhrung-erneut-durchfuhren)* an.
3. Alle Studierenden, welche die Steigungsbedingungen erfüllt haben, in die nächste Stufe und neue Zeitspanne einschreiben. Dazu bietet sich die Aktion *[Aufsteigen lassen](laufende_daten.md#einschreibung-erneuern-aufsteigen-lassen)*
4. Neue eingetrettene *[StudentInnnen](stammdaten.md#studentin)* einpflegen.
5. Die neu eingetrettenen *StudentInnen* in die neue *[Zeitspanne](stammdaten.md#zeitspanne)*, *[Stufe](#stufe)* und *[Schulklasse](stammdaten.md#schulklasse)* einschreiben.

<!-- ## Abfragen

### Welche StudentInnen sind in einer bestimmten Klasse?

### Welche Note hatte eine StudentIn in einem Kurs?

### In welchen Semstern war eine StudentIn an unserer Schule?

### Welche Kurse haben in einem vergangenen Semster stattgefunden?

### Welche StudentInnen haben an einer bestimmten Kursdurchführung teilgenommen?

### Hat ein bestimmter Student ein Semster wiederholt?
 -->


