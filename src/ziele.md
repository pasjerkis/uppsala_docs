## Ziele

### Basis

* Verwaltung der Kopfdaten von Studierenden
* Automatische Erzeugung von Studienbestätigungen als PDF.
* Abbildung des administrativen Schuljahrs mit Semestern (Zeitspanne) und Fächern
* Rückverfolgbarkeit der schulischen Laufbahn von Studierenden (Liste aller eingeschriebenen Semester)
* Erfassung der Noten und Leistungsnachweisen der Studierenden
* Erfassung von An- und Abwesenheiten der Studierenden
* Automatische Erzeugung von Zeugnissen als PDF unter Einbezug der Noten und Anwesenheiten.

### Rechnungen

* Automatische Erzeugung von QR-Rechnungen als PDF unter Einbezug der bezogenen Leistungen.


