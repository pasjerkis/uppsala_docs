# Einstellungen

### Allgemeine Einstellungen

In den allgemeinen Einstellungen lässt sich die aktuelle *[Zeitspanne](#zeitspanne)* hinterlegen. Alle Filter werden standardmässig auf diese *Zeitspanne* gesetzt.

Weiter sind die Unterschriftszeilen für die verschiedenen Dokumente hinterlegt. 

[![Allgemeine Einstellungen](images/school_settings.png)](images/school_settings.png)

### Rechnungseinstellungen

In den Rechnungseinstellungen können alle für die Erstellung einer Rechnung relevanten Daten hinterlegt werden.

[![Rechnungseinstellungen](images/school_bill_settings.png)](images/school_bill_settings.png)


