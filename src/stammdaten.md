# Stammdaten

Die Stammdaten haben keine Referenzen zu anderen Daten und beziehen sich nicht auf ein laufendes Semester (Zeitspanne). Stammdaten müssen vor der Erfassung von laufenden Daten eingepflegt werden.

## StudentIn

In der Tabelle *StudentIn* sind die Kopfdaten einer Person hinterlegt.

[![StudentIn anlegen](images/studentin.gif)](images/studentin.gif)

## Schulklasse

Die Tabelle *Schulklasse* listet alle verfügbaren Schulklassen. Eine Schulklasse ist nur eine Bezeichnung für eine Gruppe von StudentInnen. Die Zuweisung einer *[StudentIn](#studentin)* zu einer Schulklasse geschieht mit einer *[Einschreibung](laufende_daten.md#einschreibung)*.

## Kurs

Die Tabelle *Kurs* beinhaltet alle an der Schule angebotenen Fächer. Die Zuweisung eines *Kurses* zu einer Klasse geschieht mittels einer *[Kursdurchführung](#kursdurchführung)* und einer *[Einschreibung](laufende_daten.md#einschreibung)*.


## Zeitspanne

Eine *Zeitspanne* definiert eine administrative Einheit, üblicherweise ein Semester. Auch Wochenenden (zB. in einem Seminarhotel) oder Jahre (zB. in einer Primarschule) sind denkbar.
Jede *Zeitspanne* besitzt eine Ordnungszahl, sodass die Zeitspannen eine definiert Abfolge erlangen. Haben Studierende eine Zeitspanne erfolgreich abgeschlossen, können sie so in die nächste *Zeitspanne* eingeschrieben werden, siehe [Neues Semester](neues_semester.md).

[![Eintrag Zeitspanne](images/term.png)](images/term.png)

Mittels der Einstellung *Archiv* kann eine vergangene *Zeitspanne* archiviert werden. Sie erscheint in der Auswahl dann nicht mehr ganz oben, sondern unten. Nachfolgend ist dieses Verhalten dargestellt. Die nicht-archvierten *Zeitspannen* erscheinen in der Auswahl zuoberst, die Archivierten danach. Die Reihenfolge ist durch die Ordnungszahl definiert.

[![Auswahl Zeitspanne](images/term_selection.png)](images/term_selection.png)

## Stufe

Eine *Stufe* beschreibt einen Abschnitt einer Ausbildung über mehrere *Zeitspannen* hinweg. Typische *Stufen* sind zB. `Semester 1`, `Semester 2` usw.
Ein *[Kurs](stammdaten.md#kurs)* wird mittels einer *[Kursdurchführung](laufende_daten.md#kursdurchführung)* einer *Stufe* zugewiesen.

```admonish help title="Beispiel"
Das Fach (*Kurs*) `Englisch` wird in der *Stufe* `Semester 1` und `Semester 2` durchgeführt.
```

*Stufen* können verkettet werden. Dies erlaubt ein Studium als eine Abfolge von *Stufen* abzubilden. Diese Verkettung von *Stufen* erlaubt *Uppsala* die [automatische Einschreibung in eine neue *Stufe*](#einschreibung-erneuern-aufsteigen-lassen).


```admonish help title="Beispiel"
Eine Ausbildung könnte über mehrere Module verteilt sein und wird in folgenden Abschnitten durchlaufen: `Modul 1` ➔ `Modul 2` ➔ `Modul 3` ➔ `Abschlussprüfungen`
```

Jeder dieser Abschnitte entspricht einer *Stufe*.

[![Eintrag Stufe](images/level.png)](images/level.png)
